## Data import and view

In order to see all the code click on `Code` in upper right and choose "Show all code".

`getwd()` - This command return the current working directory of R session. all files read, save etc will happen in that directory by default.

`setwd("path/to/the/right/directory")` - Change (set) working directory. Windows users should use backslash`/` to seperate directories and not normal slash (`\`) as default in windows.

`read.csv("filename.csv")` - read CSV (Comma Seperated Values) file. File name should be between qutotaion mark. by default it take the first line as header (or names) to the columns.

`View(variable name)` - Command that show nicely the variable in RStudio dedicated window. 

`vector[5]` - return the 5 th element in a vector.

`data.table[1,]` - Return the first row and all columns from a a table.

`data.table[,1]` - Return the first columns and all rows from a a table.

`names(variable)` - Return the names (or columns) of the table.

### Data
 Data file: [Iris1A.csv](https://bitbucket.org/benzzz/plotly-workshop/src/master/Iris1A.csv)
 
 Data file: [Iris2A.csv](https://bitbucket.org/benzzz/plotly-workshop/src/master/Iris2A.csv)
 
 Data file: [Iris3A.csv](https://bitbucket.org/benzzz/plotly-workshop/src/master/Iris3A.csv)
